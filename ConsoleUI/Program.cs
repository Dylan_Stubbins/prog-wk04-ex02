﻿using System;

namespace ConsoleUI
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var menu = 5;

			do
			{
				Console.Clear();

				Console.WriteLine($"******************************");
				Console.WriteLine($"          MAIN MENU           ");
				Console.WriteLine($"                              ");
				Console.WriteLine($"    1. Convert KM to Miles    ");
				Console.WriteLine($"    2. Convert Miles to KM    ");
				Console.WriteLine($"    3. Option 03              ");
				Console.WriteLine($"    4. Option 04              ");
				Console.WriteLine($"                              ");
				Console.WriteLine($"******************************");
				Console.WriteLine();
				Console.WriteLine($"Please select an option");
				menu = int.Parse(Console.ReadLine());

				if (menu == 1)
				{
					Console.Clear();

					Console.WriteLine($"******************************");
					Console.WriteLine($"          Menu 01             ");
					Console.WriteLine($"                              ");
					Console.WriteLine($"    1. Option 05              ");
					Console.WriteLine($"    2. Option 06              ");
					Console.WriteLine($"    3. Option 07              ");
					Console.WriteLine($"    4. Option 08              ");
					Console.WriteLine($"                              ");
					Console.WriteLine($"    5. Main Menu              ");
					Console.WriteLine($"******************************");
					Console.WriteLine();
					Console.WriteLine($"Please select an option");
					menu = int.Parse(Console.ReadLine());
				}

				if (menu == 2)
				{
					Console.Clear();

					Console.WriteLine($"******************************");
					Console.WriteLine($"          Menu 02             ");
					Console.WriteLine($"                              ");
					Console.WriteLine($"    a. Option 09              ");
					Console.WriteLine($"    b. Option 10              ");
					Console.WriteLine($"    c. Option 11              ");
					Console.WriteLine($"    d. Option 12              ");
					Console.WriteLine($"                              ");
					Console.WriteLine($"    5. Main Menu              ");
					Console.WriteLine($"******************************");
					Console.WriteLine();
					Console.WriteLine($"Please select an option");
					var temp = Console.ReadLine();

					if (temp == "5")
					{
						menu = int.Parse(temp);
					}
					else if(temp != "5")
					{
						if (temp == "a")
						{
							Console.Clear();
							Console.WriteLine($"This is the result of option 09");
							Console.WriteLine($"Press <5> to go back to the main menu.");
							menu = int.Parse(Console.ReadLine());
						}
					}

				}

			} while (menu == 5);

			if (menu > 5)
			{
				Console.WriteLine($"Please choose the correct option");
				Console.WriteLine($"Press <ENTER> to return to the main menu");
				Console.ReadLine();
				menu = 5;
				Console.Clear();
			}
		}
	}
}
